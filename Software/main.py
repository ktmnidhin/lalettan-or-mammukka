from PyQt5.QtWidgets import *
from PyQt5.QtCore import QEvent
from PyQt5 import uic
from PyQt5.QtGui import QIcon, QPixmap
import sys, time, traceback
# from PyQt5.QtCore import QThread, pyqtSignal
from PyQt5.QtCore import *
from pathlib import Path

from joblib import load
import librosa
import audiosegment
from pydub.playback import play
import numpy as np

from pygame import mixer

 
class UI(QMainWindow):
    def __init__(self):
        self.ml = ML()
        self.threadpool = QThreadPool()
        mixer.init()
        
        
        super(UI, self).__init__()
        self.ui = uic.loadUi("main.ui", self)
        self.ui.aboutButton.clicked.connect(self.clickedBtn)

        self.ui.labelToDrop.setAcceptDrops(True)
        self.ui.labelToDrop.installEventFilter(self)

        self.ui.buttonPlay.clicked.connect(self.clickedPlay)
        self.ui.buttonPause.clicked.connect(self.clickedPause)
        pixmap = QPixmap('pic.jpeg')
        self.ui.labelToDrop.setPixmap(pixmap)
        
        self.ui.info.setText("Info:")
        self.show()

    def eventFilter(self, o, e):
        if e.type() == QEvent.DragEnter: 
            e.acceptProposedAction()
            return True
        if e.type() == QEvent.Drop:
            url = e.mimeData().urls()[0]
            self.ui.info.setText("Info: Processing"+url.toLocalFile()+"..........")
            if Path(url.toLocalFile()).stat().st_size < 150000000:
                self.ui.labelToDrop.setAcceptDrops(False)
                pixmap = QPixmap('pic.jpeg')
                self.ui.labelToDrop.setPixmap(pixmap)
                self.ui.result.setText('') 
                #####################################
                worker = Worker(self.ml.test,url.toLocalFile())
                worker.signals.result.connect(self.workerResultHandler)
                worker.signals.error.connect(self.workerExceptionHandler)
                self.threadpool.start(worker)
                #####################################
                return True
            else:
                QMessageBox.about(self, "An Error occoured!", "File size should be less than 100MB!")
        return False
    def workerResultHandler(self,result):
        print("I got result:",result)
        self.ui.result.setText(result[0]) 
        if result[0] == 'MAMMUTTY' :
            pixmap = QPixmap('Mammutty.jpg')
        if result[0] == 'MOHANLAL' :
            pixmap = QPixmap('Mohanlal.jpg')
        self.ui.labelToDrop.setPixmap(pixmap)
        self.ui.labelToDrop.setAcceptDrops(True)
        self.ui.info.setText("Info: Job finished.")
        
    def workerExceptionHandler(self,error):
        print("I got error:",error)
        self.ui.labelToDrop.setAcceptDrops(True)
        QMessageBox.about(self, "An Error occoured!", "Unsupported operation!")



    def clickedBtn(self):
        abt = About()

        

    def clickedPlay(self):
        mixer.music.unpause()
        

    def clickedPause(self):
        mixer.music.pause()
        
 
 
class ML():
    def __init__(self):
        self.model = load('mlpModel.mdl') 
    
    def get_features(self,file):
        
        data, sample_rate = librosa.load(file, sr=None)

        result=np.array([])

        mfccs=np.mean(librosa.feature.mfcc(y=data, sr=sample_rate, n_mfcc=40).T, axis=0)
        result=np.hstack((result, mfccs))

        stft=np.abs(librosa.stft(data))
        chroma=np.mean(librosa.feature.chroma_stft(S=stft, sr=sample_rate).T,axis=0)
        result=np.hstack((result, chroma))

        mel=np.mean(librosa.feature.melspectrogram(data, sr=sample_rate).T,axis=0)
        result=np.hstack((result, mel))

        return result

    def test(self, file): # split and max count
        X = []
        audio = audiosegment.from_file(file)#.resample(sample_rate_Hz=sample_rate, sample_width=2, channels=1)
        audio.export('/tmp/tmp_aud.wav', format='wav')
        mixer.music.load('/tmp/tmp_aud.wav')
        mixer.music.play()
        audio = audio.filter_silence(duration_s=0.2, threshold_percentage=5.0)
        for i in range(int(audio.duration_seconds/5)+1):
            t1 = i*5000
            t2 = t1+5000
            trimmedAudio = audio[t1:t2]
            outname_silence = "/tmp/tst.wav"
            trimmedAudio.export(outname_silence, format="wav")
            X.append(self.get_features(outname_silence))
        y = self.model.predict(X)
        unique,pos = np.unique(y,return_inverse=True)
        counts = np.bincount(pos)  
        maxpos = counts.argmax()  
        name, count = (unique[maxpos],counts[maxpos])
        confidence = (count/y.size)*100
        print("Test Finished")
        return name , confidence

################### For threading ###################################

class WorkerSignals(QObject):
    finished = pyqtSignal()
    error = pyqtSignal(tuple)
    result = pyqtSignal(object)
    progress = pyqtSignal(int)

class Worker(QRunnable):
    def __init__(self, fn, *args, **kwargs):
        super(Worker, self).__init__()
        self.fn = fn
        self.args = args
        self.kwargs = kwargs
        self.signals = WorkerSignals()    

    @pyqtSlot()
    def run(self):
        try:
            result = self.fn(*self.args, **self.kwargs)
        except:
            # traceback.print_exc()
            exctype, value = sys.exc_info()[:2]            
            self.signals.error.emit((exctype, value, traceback.format_exc()))
        else:
            self.signals.result.emit(result)
        finally:
            self.signals.finished.emit() 
######################################################
class About(QDialog):
     def __init__(self, parent=None):
        super(About, self).__init__()
        self.about = uic.loadUi("about.ui", self)
        self.show()


app = QApplication(sys.argv)
window = UI()
app.exec_()
# Sound Classification Using ML

Project submitted by Nidhin Jose <nidhin84@gmail.com>

## Overview

During the lockdown days ICFOSS conducted a course on Machine Learning Through FOSS. From this 21 hr course we learned the basics of python, python libraries such as pandas, numpy, matplotlib, sklearn etc and basic theories of machine learning. As final project I choose sound classification using ML.



## Goals

* Study physics behind sound and its mathematical formulation.
* Analyse digital audio file.
* Study features of sounds.
* Study how to extract features of audio files through FOSS.
* Collect sound clips and execute the algorithms.
* Study the effectiveness of different ML algorithms.
* Select the best algorithm.
* Make an ML model.
* Using this model develop a software that can classify sound clips.

## Work Report
* I have chosen the two legend malayalam movie actor’s (Mr. Mohanlal and Mr. Mammutty) voices for classification process.
* Collected sound clips of these actors from different sources.
* Made a data set of collected files with label
* Using audiosegment python library preprocessed the audio files and made 5s audio clips on the fly.
* Using librosa python library extracted the MFCC, chroma_stft, mel spectrogram features from each  audio clips.
* Created a pandas Dataframe using these features and labeled.
* Splitted the data in to train test sets.
* Trained the machine using MLPClassifier, DecisionTreeClassifier, SVM and  RandomForestClassifier.
* Studied the effectiveness.
* Best result obtained from MLPClassifier and svmModel.
* Made a pickle of these model Objects using joblib python library’s dump function.
* Created a GUI application and loaded the pickled model.
* In the GUI you can drag and drop sound clip or video clip files less than 150Mb containing speech of Mr. Mammootty or Mr. Mohanlal.
System will analyze the clip and display the Photograph and name of the actor.

## Screenshot

![Screenshot](misc/screenshot.png)

## License
This project is licensed under the GNU General Public License v3.0.
